# PyMODg
PyMODg is a forward modelling package written in Python and specifically 
designed to model gravitational attraction for specific problems. 

## Building 3D mesh
Mesh tools are used to build a 3D mesh gathering available information such as:
- topography (in the form of a DEM raster)
- simple geology
- water table
- areas of interest (in the form of 3D bodies)

## Computing gravitational attraction at specific locations
The vertical component of the gravitational attraction can be calculated at 
specific locations following different appraoch:
- Prism approach
- Point-mass approximation
- Mac Millian approach

## Installation
This package currently works with Python 3.6. We strongly suggest using it under
a separate conda environment. 
To install a spearate environment using anaconda simply run the following commands in anaconda prompt:

`conda create -n myenv python=3.7`

`conda activate myenv`

`conda install pandas numpy gdal vtk`

Then clone the pymodg repository and start using it locally.

## Python dependencies
- vtk
- numpy
- pandas
- gdal

Developed by Arnaud Watlet (arnw@bgs.ac.uk)
