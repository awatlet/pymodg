
from vtk import *
import vtk
import numpy as np
from vtk.util import numpy_support
import pandas as pd
import numpy as np
# import gdal
import os
from pymodg.utils import *
from pymodg.grid_dev import *
from pymodg.gravi import *

dem_list = ['examples/mnt_rch_ign_middle_400m_10m_clipped.tif']#'examples/mnt_rch_ign_middle_100m_1m_clipped.tif',
mygrid = multigrid3d_dev(dem_list=dem_list,z_boundary=[['max',150],['max',150]],y_boundary=['auto','auto'])#,x_boundary=[[94000,94350],'auto'])

dem_bottom = 'examples/cave_bottom_new2.tif'
dem_top = 'examples/cave_top_new3.tif'
mygrid.set_internal_regions(bottom=dem_bottom,top=dem_top)

mygrid.cell_properties = pd.DataFrame(np.zeros(len(mygrid.cell_centers.index)), index=mygrid.cell_centers.index)
# mygrid.cell_properties['geology'] = mygrid.internal_regions['geology'].region.values
mygrid.cell_properties['cavity'] = mygrid.internal_regions['cavity'].region.values
# mygrid.cell_properties.SZ = distance.values
mygrid.cell_properties['mesh_num'] = mygrid.grid_num_per_cell
mygrid.create_hexahedral_mesh(grids_num=[0])
mygrid.save('test.vtk',data = mygrid.cell_properties.iloc[:,1:])

import pyvista as pv

# read the data
file_name = "test_0.vtk"  # minimal example vtk file
grid = pv.read(file_name)

# plot the data with an automatically created Plotter
grid.plot(show_scalar_bar=False, show_axes=False)


# reader = mygrid.vtk
# # Read the source file.
# # reader = vtkUnstructuredGridReader()
# # reader.SetFileName(file_name)
# # reader.Update()  # Needed because of GetScalarRange
# output = reader.GetOutput()
# output_port = reader.GetOutputPort()
# scalar_range = output.GetScalarRange()
#
# # Create the mapper that corresponds the objects of the vtk file
# # into graphics elements
# mapper = vtkDataSetMapper()
# mapper.SetInputConnection(output_port)
# mapper.SetScalarRange(scalar_range)
#
# # Create the Actor
# actor = vtkActor()
# actor.SetMapper(mapper)
#
# # Create the Renderer
# renderer = vtkRenderer()
# renderer.AddActor(actor)
# renderer.SetBackground(1, 1, 1) # Set background to white
#
# # Create the RendererWindow
# renderer_window = vtkRenderWindow()
# renderer_window.AddRenderer(renderer)
#
# # Create the RendererWindowInteractor and display the vtk_file
# interactor = vtkRenderWindowInteractor()
# interactor.SetRenderWindow(renderer_window)
# interactor.Initialize()
# interactor.Start()