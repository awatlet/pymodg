__author__ = 'Arnaud Watlet'

from vtk import *
import numpy as np
from vtk.util import numpy_support
import pandas as pd
import numpy as np
from osgeo import gdal
import os

def read_raster(raster,raster_path='', x_offset=0.0, y_offset=0.0, field_offset=0.0):
    ds = None
    del ds
    if raster_path == '':
        raster_path = raster
    if isinstance(raster, str):
        gdal.UseExceptions()
        ds = gdal.Open(raster)

    elif isinstance(raster, tuple):
        return raster
    else:
        ds = raster
    band = ds.GetRasterBand(1)
    field = band.ReadAsArray() + field_offset
    z1 = field.flatten()
    nrows, ncols = field.shape
    imx0, imdx, imdxdy, imy0, imdydx, imdy = ds.GetGeoTransform()
    imx0 = imx0 - x_offset
    imy0 = imy0 - y_offset
    imx1 = np.round(imx0, 2) + np.round(imdx, 2) * ncols
    imy1 = np.round(imy0, 2) + np.round(imdy, 2) * nrows
    #     x = np.arange(imx0,imx1,imdx)
    #     y = np.arange(imy0,imy1,imdy)
    x = np.round(np.linspace(np.round(imx0, 2), imx1 - imdx, ncols), 2)
    y = np.round(np.linspace(np.round(imy0, 2), imy1 - imdy, nrows), 2)
    x, y = np.meshgrid(x, y)
    x1 = x.flatten()
    y1 = y.flatten()
    return np.array([x, y, field]), [[np.round(imx0, 2), np.round(imy0, 2)], [imx1, imy1]], [np.round(imdx, 2),
                                                                                             np.round(imdy, 2)],raster_path
def distance_to_dem(dem,cell_centers_rev):
    if isinstance(dem, str):
        grid_dem,grid_dem_bounds,grid_dem_d = read_raster(dem)
    else:
        grid_dem,grid_dem_bounds,grid_dem_d = dem
    x_dem,y_dem,z_dem = grid_dem
    points_dem = np.array([x_dem.flatten(),y_dem.flatten(),z_dem.flatten()]).T
    dem_grid = pd.DataFrame(index=pd.MultiIndex.from_tuples([tuple(x) for x in points_dem[:,:2].round(2)]), columns=['dem'])
    dem_grid.dem = points_dem[:,2]
#     cell_xy_tuples = [tuple(x) for x in np.array(cell_centers.tolist())[:,:2]]
#     cell_centers_rev = pd.DataFrame(np.array([np.array(cell_centers.tolist())[:,2],np.array(cell_centers.index)]).T,index=pd.MultiIndex.from_tuples(cell_xy_tuples),columns=['z','idx'],)
    mapped_dem_grid = dem_grid.loc[cell_centers_rev.index]
    distance = (cell_centers_rev.z - mapped_dem_grid.dem)
    return distance

def find_offset_between_rasters(raster1, raster2):
    gdal.UseExceptions()
    if isinstance(raster1,str):
        ds = gdal.Open(raster1)
    else:
        ds = raster1
    imx0_1, imdx_1, imdxdy_1, imy0_1, imdydx_1, imdy_1 = ds.GetGeoTransform()
    gdal.UseExceptions()
    if isinstance(raster2,str):
        ds = gdal.Open(raster2)
    else:
        ds = raster2
    imx0_2, imdx_2, imdxdy_2, imy0_2, imdydx_2, imdy_2 = ds.GetGeoTransform()
    x_offset = imx0_1 - imx0_2
    y_offset = imy0_1 - imy0_2
    return x_offset, y_offset


def find_cavities(dem_bottom, dem_top, cell_centers_rev):
    if isinstance(dem_bottom, str):
        grid_bottom, grid_bottom_bounds, grid_bottom_d, grid_bottom_path = read_raster(dem_bottom)
    else:
        grid_bottom, grid_bottom_bounds, grid_bottom_d, grid_bottom_path = dem_bottom

    if isinstance(dem_top, str):
        grid_top, grid_top_bounds, grid_top_d, grid_top_path = read_raster(dem_top)
    else:
        grid_top, grid_top_bounds, grid_top_d, grid_top_path = dem_top

    x_bottom, y_bottom, z_bottom = grid_bottom
    x_top, y_top, z_top = grid_top

    points_bottom = np.array([x_bottom.flatten(), y_bottom.flatten(), z_bottom.flatten()]).T
    points_top = np.array([x_top.flatten(), y_top.flatten(), z_top.flatten()]).T
    points_bottom = points_bottom[(points_bottom[:, 2] != 0.0) & (points_bottom[:, 2] != 1.0)]
    points_top = points_top[(points_top[:, 2] != 0.0) & (points_top[:, 2] != 1.0)]
    if len(points_top) == 0 and len(points_top) == 0:
        return []
    else:
        cavity_grid = pd.DataFrame(index=pd.MultiIndex.from_tuples([tuple(x) - np.array(grid_bottom_d) / 2 for x in points_bottom[:, :2].round()]),
                                   columns=['bottom', 'top'])
        cavity_grid.bottom = points_bottom[:, 2]
        cavity_grid.top = points_top[:, 2]
        #     cell_xy_tuples = [tuple(x) for x in np.array(cell_centers.tolist())[:,:2]]
        #     cell_centers_rev = pd.DataFrame(np.array([np.array(cell_centers.tolist())[:,2],np.array(cell_centers.index)]).T,index=pd.MultiIndex.from_tuples(cell_xy_tuples),columns=['z','idx'],)
        mapped_cell_centers = cell_centers_rev.loc[cavity_grid.index]
        mapped_cavity_grid = cavity_grid.loc[mapped_cell_centers.index]
        mapped_cell_centers['masked'] = (
        (mapped_cell_centers.z > mapped_cavity_grid.bottom) & (mapped_cell_centers.z < mapped_cavity_grid.top))

        return np.array([int(x) for x in mapped_cell_centers[mapped_cell_centers.masked].idx.values])



def map_regions_boundaries(raster, cell_centers_surface):
    if isinstance(raster, str):
        grid_regions, grid_regions_bounds, grid_regions_d, grid_regions_path = read_raster(raster)
    else:
        grid_regions, grid_regions_bounds, grid_regions_d, grid_regions_path = raster
    x_regions, y_regions, field_regions = grid_regions
    # print(grid_regions[2])
    points_regions = np.array([x_regions.flatten(), y_regions.flatten(), np.zeros(len(x_regions.flatten()))]).T
    regions_grid = pd.DataFrame(index=pd.MultiIndex.from_tuples([tuple(np.array(x) - np.array(grid_regions_d) / 2) for x in points_regions[:, :2].round(2)]),
                                columns=['region'])
    regions_grid.region = field_regions.flatten()
    mapped_cell_centers_tmp = pd.DataFrame(cell_centers_surface.copy())#pd.DataFrame(index=cell_centers_surface.index)
    mapped_cell_centers_tmp['region'] = np.zeros(mapped_cell_centers_tmp.shape[0])
    # print(mapped_cell_centers_tmp)
    try:
        mapped_cell_centers = mapped_cell_centers_tmp.loc[regions_grid.index].dropna(subset=['idx'])
    except:
        mapped_cell_centers = mapped_cell_centers_tmp.loc[regions_grid.index].dropna(subset=['surface'])
    # print(regions_grid.shape[0],mapped_cell_centers.shape[0])
    mapped_cell_centers['region'] = regions_grid.region
    return mapped_cell_centers  # .dropna(subset=['z'])


def translate_raster_old(raster, dip=0, direction=0, depth=100):
    dip = -dip
    raster = gdal.Open(raster, 1)
    imx0, imdx, imdxdy, imy0, imdydx, imdy = raster.GetGeoTransform()
    x1 = -depth * np.tan(np.pi / 2 - (np.pi / 180 * dip)) * np.cos(np.pi / 2 - (np.pi / 180 * direction))
    y1 = -depth * np.tan(np.pi / 2 - (np.pi / 180 * dip)) * np.sin(np.pi / 2 - (np.pi / 180 * direction))
    # print(x1,y1,)
    #     if np.abs(x1%imdx) <= np.abs(imdx/2.):
    #         imx0 = imx0 + y
    #     else :
    #         imx0 = imx0 + np.sign(x1)*imdx
    # #     imx0 = imx0 + depth*np.tan(np.pi/2-dip)*np.cos(np.pi/2-direction)
    #     if np.abs(y1%imdy) <= np.abs(imdy/2.):
    #         imy0 = imy0 + y1//imdy
    #     else :
    #         imy0 = imy0 + np.sign(y1)*imdy
    imx0 = imx0 + np.sign(x1) * (np.abs(x1) // (np.abs(imdx) ) * np.abs(imdx))
    imy0 = imy0 + np.sign(y1) * (np.abs(y1) // (np.abs(imdy) ) * np.abs(imdy))
    # print(imx0)
    #     imy0 = imy0 +depth*np.tan(np.pi/2-dip)*np.cos(np.pi/2-direction)
    #     print(x1,y1)
    #     print(np.sign(x1)*(np.abs(x1)//(np.abs(imdx)/2.)*np.abs(imdx)), np.sign(y1)*(np.abs(y1)//(np.abs(imdy)/2.)*np.abs(imdy)))
    band = raster.GetRasterBand(1)
    field = band.ReadAsArray()
    z1 = field.flatten()
    nrows, ncols = field.shape
    imx1 = imx0 + imdx * ncols
    imy1 = imy0 + imdy * nrows
    x = np.arange(imx0, imx1, imdx)
    y = np.arange(imy0, imy1, imdy)
    x, y = np.meshgrid(x, y)
    raster = None
    band = None
    return np.array([x, y, field]), [[imx0, imy0], [imx1, imy1]], [imdx, imdy],raster


def resample_raster_resolution(raster, output='', outsize=1):
    if output == '':
        output = raster
    output = ''.join(np.insert(np.array(os.path.splitext(output)), 1, '_%.2f_outsize')) % outsize

    if isinstance(raster,str):
        src_ds = gdal.Open(raster)
    else:
        src_ds = raster

    drv = gdal.GetDriverByName('GTiff')
    dst_ds = drv.Create(output,
                        int(src_ds.RasterXSize / outsize), int(src_ds.RasterYSize / outsize), 1, gdal.GDT_Float32)
    dst_ds.SetProjection(src_ds.GetProjection())
    dst_ds.SetGeoTransform(tuple(
        [src_ds.GetGeoTransform()[0], src_ds.GetGeoTransform()[1] * outsize, 0.0, src_ds.GetGeoTransform()[3], 0.0,
         src_ds.GetGeoTransform()[5] * outsize]))
    gdal.ReprojectImage(src_ds, dst_ds)
    del src_ds
    del dst_ds  # Flush
    return gdal.Open(output, gdal.GA_ReadOnly), output

def translate_raster(raster,x_off=0,y_off=0,output='',rename=True):
    if output=='':
        output=raster
    if rename:
        output = ''.join(np.insert(np.array(os.path.splitext(output)).astype('U64'), 1, '_translated_%.2f_%.2f')) % (x_off, y_off)
    if isinstance(raster,str):
        src_ds = gdal.Open(raster)
    else:
        src_ds = raster
    drv = gdal.GetDriverByName('GTiff')
    dst_ds = drv.Create(output,
                        int(src_ds.RasterXSize), int(src_ds.RasterYSize), 1, gdal.GDT_Float32)
    imx0, imdx, imdxdy, imy0, imdydx, imdy = src_ds.GetGeoTransform()
    dst_ds.SetGeoTransform((imx0 + x_off, imdx, imdxdy, imy0 + y_off, imdydx, imdy))
    dst_ds.SetProjection(src_ds.GetProjection())
    band_src = src_ds.GetRasterBand(1)
    data = band_src.ReadAsArray()
    band_dst = dst_ds.GetRasterBand(1)
    band_dst.WriteArray(data)
    # dst_ds.FlushCache()
    # del src_ds
    # del dst_ds  # Flush
    drv = None
    band_dst = None
    band_src = None
    dst_ds = None
    src_ds = None
    del src_ds
    del dst_ds  # Flush
    return output


def align_rasters(align_to_raster,align_on_raster,frame=[0,0]):
    align_to, align_to_bounds, align_to_d, align_to_path = read_raster(align_to_raster)
    align_on, align_on_bounds, align_on_d, align_on_path = read_raster(align_on_raster)
    if np.abs(align_on_d[0]) != np.abs(align_to_d[0]):
        align_to, align_to_path = resample_raster_resolution(align_to_path,
                                                                outsize=np.abs(align_on_d[0])/np.abs(align_to_d[0]))

    else:
        align_to = align_to_path
    align_on, align_on_bounds, align_on_d, align_on_path = read_raster(align_on_raster)
    # x_off, y_off = find_offset_between_rasters(align_on_raster, align_to_path)
    x_off = align_to_bounds[0][0] - align_on_bounds[0][0]
    y_off = align_to_bounds[0][1] - align_on_bounds[0][1]
    if np.abs(x_off) >= align_on_d[0] or np.abs(y_off) >= align_on_d[1]:
        # align_to_tmp, align_to_bounds, align_to_d, align_to_path_tmp = read_raster(align_to)
        srcwin = list(((np.abs(np.array(align_on_bounds[0])-np.array(align_to_bounds[0]))-np.array(frame)).astype(int) * 1 / np.abs(align_on_d))) \
                            + list(np.array([align_on.shape[2], align_on.shape[1]])+np.array(frame)* 2 / np.abs(align_on_d[0]))
        # print(srcwin)
        # align_to = crop_raster(align_to, srcWin=list(np.array(align_on_bounds[0])+np.array(frame)) + [align_on.shape[1], align_on.shape[2]])

        align_to = crop_raster(align_to_path, srcWin=srcwin)
        # align_to = gdal.Open(align_to, gdal.GA_ReadOnly)
        # x_off, y_off = find_offset_between_rasters(align_on_raster, align_to)
        align_to_tmp, align_to_bounds_tmp, align_to_d_tmp, align_to_path_tmp = read_raster(align_to)
        x_off = (align_on_bounds[0][0] - align_to_bounds_tmp[0][0])#*np.sign(align_to_d_tmp[0])
        y_off = (align_on_bounds[0][1] - align_to_bounds_tmp[0][1])#*np.sign(align_to_d_tmp[1])
    align_to_path = translate_raster(align_to, x_off-(frame[0]*np.sign(align_on_d[0])), y_off-(frame[1]*np.sign(align_on_d[1])), output=align_to_path)
    return align_to_path

# def align_rasters(align_to_raster,bounds,intervals,frame=[0,0]):
#     align_to, align_to_bounds, align_to_d, align_to_path = read_raster(align_to_raster)
#     x = np.arange(bounds[0][0] + intervals[0] / 2, bounds[1][0] + intervals[0] / 2, intervals[0])
#     y = np.arange(bounds[0][1] + intervals[1] / 2, bounds[1][1] + intervals[1] / 2, intervals[1])
#     # align_on, align_on_bounds, align_on_d, align_on_path = read_raster(align_on_raster)
#     if np.abs(intervals[0]) != np.abs(align_to_d[0]):
#         align_to, align_to_path = resample_raster_resolution(align_to_path,
#                                                                 outsize=np.abs(intervals[0])/np.abs(align_to_d[0]))
#
#     else:
#         align_to = align_to_path
#
#         srcwin = list((np.abs(np.array(bounds[0][:2]) - np.array(align_to_bounds[0]))).astype(int) * 1 / np.abs(intervals)) + [x.shape[0], y.shape[0]]
#     # align_to_tmp, align_to_bounds, align_to_d, align_to_path_tmp = read_raster(align_to)
#     #     srcwin = list(((np.abs(np.array(align_on_bounds[0])-np.array(align_to_bounds[0]))-np.array(frame)).astype(int) * 1 / np.abs(align_on_d))) \
#     #                         + list(np.array([align_on.shape[1], align_on.shape[2]])+np.array(frame)* 2 / np.abs(align_on_d[0]))
#         # print(srcwin)
#         # align_to = crop_raster(align_to, srcWin=list(np.array(align_on_bounds[0])+np.array(frame)) + [align_on.shape[1], align_on.shape[2]])
#
#         align_to = crop_raster(align_to_path, srcWin=srcwin)
#         x_off, y_off = find_offset_between_rasters(align_on_raster, align_to)
#     align_to, align_to_path = translate_raster(align_to, x_off-(frame[0]*np.sign(align_on_d[0])), y_off-(frame[1]*np.sign(align_on_d[1])), output=align_to_path)
#
#     return align_to, align_to_path

def crop_raster(raster, x_boundary=None, y_boundary=None,srcWin=None,temp='temp.tif'):
    if srcWin is None:
        to_crop, to_crop_bounds, to_crop_d, to_crop_path = read_raster(raster)
        if x_boundary is None:
            x_boundary = np.array([to_crop_bounds[0][0], to_crop_bounds[1][0]])
        if y_boundary is None:
            y_boundary = np.array([to_crop_bounds[0][1], to_crop_bounds[1][1]])
        x_boundary = np.sort(np.array(x_boundary))[::int(np.sign(to_crop_d[0]))]
        y_boundary = np.sort(np.array(y_boundary))[::int(np.sign(to_crop_d[1]))]
        x = np.arange(x_boundary[0], x_boundary[1]+to_crop_d[0], to_crop_d[0])
        y = np.arange(y_boundary[0], y_boundary[1]+to_crop_d[1], to_crop_d[1])
        # print((y_boundary[0], y_boundary[1], to_crop_d[1]))
        srcWin = list(np.array(list(
            (np.abs(np.array([x_boundary[0],y_boundary[0]]) - np.array(to_crop_bounds[0]))).astype(int) * 1 / np.abs(to_crop_d)) + [
                     x.shape[0], y.shape[0]]).astype(int))

    src_ds = gdal.Open(raster)
    cropped_raster = gdal.Translate(temp, raster,
                                          srcWin=srcWin)
    # print(srcWin)
    band_src = src_ds.GetRasterBand(1)
    data = band_src.ReadAsArray(int(srcWin[0]),int(srcWin[1]),int(srcWin[2]),int(srcWin[3]))
    # print(data)
    band_cropped = cropped_raster.GetRasterBand(1)
    band_cropped.WriteArray(data)
    # cropped_raster.FlushCache()
    band_cropped = None
    band_src = None
    src_ds = None
    cropped_raster = None
    # del src_ds
    # del cropped_raster
    # gdal.Open(temp, gdal.GA_ReadOnly)
    return temp

# def crop_raster(raster,srcWin):
#     # grid, bounds, grid_d, grid_path = read_raster(raster)
#     # srcWin = list(np.abs(np.array(srcWin)-np.array(bounds[0]+[0,0])).astype(int))
#     # srcWin[0] = srcWin[0] * 1 / np.abs(grid_d[0])
#     # srcWin[1] = srcWin[1] * 1 / np.abs(grid_d[0])
#     # print(srcWin)
#     src_ds = gdal.Open(raster)
#     cropped_raster = gdal.Translate('temp.tif', raster,
#                                           srcWin=srcWin)
#     data = src_ds.GetRasterBand(1).ReadAsArray(srcWin[0],srcWin[1],srcWin[2],srcWin[3])
#     cropped_raster.GetRasterBand(1).WriteArray(data)
#     cropped_raster.FlushCache()
#     del src_ds
#     del cropped_raster
#     return gdal.Open('temp.tif', gdal.GA_ReadOnly)

def set_corners(centers,size_xyz=[1,1,1]):
    order = np.array([[-1, -1, -1], [1, -1, -1], [1, 1, -1], [-1, 1, -1], [-1, -1, 1], [1, -1, 1], [1, 1, 1],
                      [-1, 1, 1]]) * np.array(size_xyz) / 2
    corner_list = np.array([centers + o for o in order])
    # corner_list = np.array([corner_list[:, c] for c in range(corner_list.shape[1])])
    return corner_list

def build_nodes(x,y,z,shape=1.0):
    x_offset=np.array([-1,-1,-1,-1,1,1,1,1])*shape/2
    y_offset=np.array([1,1,-1,-1,1,1,-1,-1])*shape/2
    z_offset=np.array([1,-1,-1,1,1,-1,-1,1])*shape/2
#     return pd.DataFrame([list(np.array([x_offset+x,y_offset+y,z_offset+z]).T)])
    return np.array([x_offset+x,y_offset+y,z_offset+z]).T