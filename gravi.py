__author__ = 'Arnaud Watlet'

from vtk import *
import numpy as np
from vtk.util import numpy_support
import pandas as pd
import numpy as np
from osgeo import gdal
import os


def cube_gravitational_attraction(center,cell_nodes,rho=1000,G=6.67*10**(-11)):
    x0, y0, z0 = center
    cell_nodes = np.array(cell_nodes.tolist())
    x_corners = cell_nodes[:,0]
    y_corners = cell_nodes[:,1]
    z_corners = cell_nodes[:,2]
    vcorr = []
    for j in range(len(x_corners)):
        if x0 - x_corners[j] == np.max(x0 - x_corners):
            ii = 2
        else:
            ii = 1
        if y0 - y_corners[j] == np.max(y0 - y_corners):
            ji = 2
        else:
            ji = 1
        if z0 - z_corners[j] == np.max(z0 - z_corners):
            ki = 2
        else:
            ki = 1
#         print(ii,ji,ki)
        #print(x_corners[j],y_corners[j],z_corners[j])
        dx = (x_corners[j] - x0)
        dy = (y_corners[j] - y0)
        dz = (z_corners[j] - z0)

        distance_xz = np.sqrt((x_corners[j] - x0)**2 + (z_corners[j] - z0)**2)
        distance_yz = np.sqrt((z_corners[j] - z0)**2 + (y_corners[j] - y0)**2)
        distance_xyz = np.sqrt((x_corners[j] - x0)**2 + (y_corners[j] - y0)**2 + (z_corners[j] - z0)**2)
        # vcorr.append((-1)**(ii[j]+ji[j]+ki[j])* (dy * np.log((dx+distance_xyz)/distance_yz) + dx * np.log((dy+distance_xyz)/distance_xz) - dz * np.arctan(dx*dy/(dz*distance_xyz))))
        vcorr.append(((-1)**(ii))*((-1)**ji)*((-1)**ki)* (dy * np.log((dx+distance_xyz)) + dx * np.log((dy+distance_xyz)) + 2*dz * np.arctan((dx+dy+distance_xyz)/dz))) # - dz*np.arctan((dx*dy)/(dz*distance_xyz)))) #- dz*np.arcsin((dy**2 + dz**2 + dy*distance_xyz)/((dy + distance_xyz)*distance_yz)))) #2*dz * np.arctan((dx+dy+distance_xyz)/dz)))

    return np.array(vcorr).sum()* (-G) * rho


def point_mass(center,point,size=1,rho=1000,G=6.67*10**(-11)):
    dx = (point[0] - center[0])
    dy = (point[0] - center[0])
    dz = (point[2] - center[2])
    d = np.sqrt(dx**2+dy**2+dz**2)
    dg = G*rho*size*(-dz/d**3)
    return dg


def point_mass_df(center,grid,size=1,rho=1000,G=6.67*10**(-11)):
    return G*rho*size*(-(grid.z-center[2])/np.sqrt((grid.x-center[0])**2+(grid.y-center[1])**2+(grid.z-center[2])**2)**3)

def mac_millan_df(center,grid,size=1,rho=1000,G=6.67*10**(-11)):
    d = np.sqrt((grid.x-center[0])**2+(grid.y-center[1])**2+(grid.z-center[2])**2)
    alpha = 2 * (grid.x-center[0])**2 - (grid.y-center[1])**2 - (grid.z-center[2])**2
    beta = (grid.x-center[0])**2 + 2 * (grid.y-center[1])**2 - (grid.z-center[2])**2
    omega = -(grid.x-center[0])**2 - (grid.y-center[1])**2 + 2 * (grid.z-center[2])**2
    return G * rho * size * (-(grid.z-center[2])/d**3 - 5/24 * (alpha*(grid.x-center[0])**2 + beta*(grid.y-center[1])**2 + omega*(grid.z-center[2])**2)*(grid.z-center[2])/d**7 + 1/12 * omega*(grid.z-center[2])/d**5)