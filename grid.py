__author__ = 'Arnaud Watlet'

from vtk import *
import numpy as np
from vtk.util import numpy_support
import pandas as pd
import numpy as np
from osgeo import gdal
import os
from pymodg.utils import read_raster, find_cavities, map_regions_boundaries, set_corners, crop_raster, \
                                align_rasters, translate_raster_old
from pymodg.gravi import point_mass_df,cube_gravitational_attraction, mac_millan_df

class multigrid3d():
    def __init__(self, bounds_list=None, intervals_list=None, dem_list=None, z_boundary=None, x_boundary=None,
                                        y_boundary=None, grid_num=None, topo_layer=None, flat=False):

        if dem_list is None:
            flat = True
            if bounds_list is None or intervals_list is None:
                print('Missing boundaries and/or intervals')
                return

        else:
            # flat = False
            bounds_list, intervals_list = [], []
            if z_boundary is None:
                z_boundary = ['auto'] * len(dem_list)
            if x_boundary is None:
                x_boundary = ['auto'] * len(dem_list)
            if y_boundary is None:
                y_boundary = ['auto'] * len(dem_list)
            if topo_layer is None:
                topo_layer = [None] * len(dem_list)
            for d in range(len(dem_list)):
                dem = dem_list[d]

                grid_dem, grid_dem_bounds, grid_dem_d, grid_dem_path = read_raster(dem)


                x_bound, y_bound, z_bound = x_boundary[d], y_boundary[d], z_boundary[d]

                if z_boundary[d] == 'auto':
                    z_bound = [grid_dem[2].flatten().min()-np.abs(grid_dem_d[0]),grid_dem[2].flatten().max()]
                else:
                    if 'max' in z_boundary[d]:
                        z_bound[z_bound.index('max')] = grid_dem[2].flatten().max()
                    if 'min' in z_boundary[d]:
                        z_bound[z_bound.index('min')] = grid_dem[2].flatten().min()
                if x_boundary[d] == 'auto':
                    # x_bound = [grid_dem[0].flatten().min()+(grid_dem_d[0]-np.abs(grid_dem_d[0]))/(2*np.abs(grid_dem_d[0])),grid_dem[0].flatten().max()+(grid_dem_d[0]+np.abs(grid_dem_d[0]))/(2*np.abs(grid_dem_d[0]))]
                    x_bound = [grid_dem[0].flatten().min(), grid_dem[0].flatten().max()]
                else:
                    if 'max' in x_boundary[d]:
                        x_bound[x_bound.index('max')] = grid_dem[0].flatten().max()#+(grid_dem_d[0]+np.abs(grid_dem_d[0]))/(2*np.abs(grid_dem_d[0]))
                    if 'min' in x_boundary[d]:
                        x_bound[x_bound.index('min')] = grid_dem[0].flatten().min()#+(grid_dem_d[0]-np.abs(grid_dem_d[0]))/(2*np.abs(grid_dem_d[0]))

                if y_boundary[d] == 'auto':
                    # y_bound = [grid_dem[1].flatten().min()+(grid_dem_d[1]-np.abs(grid_dem_d[1]))/(2*np.abs(grid_dem_d[1])),grid_dem[1].flatten().max()+grid_dem_d[0]+(grid_dem_d[1]+np.abs(grid_dem_d[1]))/(2*np.abs(grid_dem_d[1]))]
                    y_bound = [grid_dem[1].flatten().min(), grid_dem[1].flatten().max()]
                else:
                    if 'max' in y_boundary[d]:
                        y_bound[y_bound.index('max')] = grid_dem[1].flatten().max()#+(grid_dem_d[1]+np.abs(grid_dem_d[1]))/(2*np.abs(grid_dem_d[1]))
                    if 'min' in y_boundary[d]:
                        y_bound[y_bound.index('min')] = grid_dem[1].flatten().min()#+(grid_dem_d[1]-np.abs(grid_dem_d[1]))/(2*np.abs(grid_dem_d[1]))
                # print(np.sort(np.array(y_bound)))
                z_bound = np.sort(np.array(z_bound))[::-1]
                x_bound = np.sort(np.array(x_bound))[::int(np.sign(grid_dem_d[0]))]
                y_bound = np.sort(np.array(y_bound))[::int(np.sign(grid_dem_d[1]))]
                print(z_bound)
                bound_1 = [x_bound[0],y_bound[0],z_bound[0]]
                bound_2 = [x_bound[1], y_bound[1], z_bound[1]]
                # print(bound_1,bound_2)
                grid_dem_d.append(-np.abs(grid_dem_d[0]))
                bounds_list.append([bound_1, bound_2])
                intervals_list.append(grid_dem_d)
        self.grid_size = [np.abs(np.prod(np.array(interval))) for interval in intervals_list]
        self.bounds = np.array(bounds_list)
        self.intervals = np.array(intervals_list)
        self.vtk = None

        central = True
        dem_new_list = []
        for d in range(len(bounds_list)):

            bounds = bounds_list[d]
            dx, dy, dz = intervals_list[d]
            print('Meshgridding grid number %i with cell size %.2f x %.2f x %.2f m' % (d, dx, dx, dx))
            x = np.arange(bounds[0][0] + dx / 2, bounds[1][0] + 3*dx / 2, dx)
            y = np.arange(bounds[0][1] + dy / 2, bounds[1][1] + 3*dy / 2, dy)
            z = np.arange(bounds[0][2] + dz / 2, bounds[1][2] + 3*dz / 2, dz)
            grid = np.array(np.meshgrid(x, y, z, indexing='xy'))
            print(grid)
            if central:
                self.grids = [grid]
                self.cell_centers = grid.T.flatten().reshape(-1, 3)
                self.cell_size = np.abs(np.ones(self.cell_centers.shape[0]) * dx * dy * dz)
                if grid_num is None:
                    grid_num = np.arange(len(bounds_list))
                self.grid_num_per_cell = np.ones(self.cell_centers.shape[0]) * grid_num[0]

            else:
                self.grids.append(grid)
                points_tmp = grid.T.flatten().reshape(-1, 3)

                if flat:
                    points_flat = points_tmp[~(((points_tmp[:, 0] > x.min()) & (points_tmp[:, 0] < x.max())) & (
                        (points_tmp[:, 1] > y.min()) & (points_tmp[:, 1] < y.max())) & (
                                                   (points_tmp[:, 2] > z.min()) & (points_tmp[:, 2] < z.max())))]
                    self.cell_centers = np.concatenate([self.cell_centers, points_flat])
                    del points_flat
                    # self.points_flat = self.cell_centers
                else:
                    self.cell_centers = np.concatenate([self.cell_centers, points_tmp])
                    # self.points_flat = np.concatenate([self.cell_centers, points_flat])


                self.cell_size = np.concatenate([self.cell_size, np.abs(np.ones(points_tmp.shape[0]) * dx * dy * dz)])
                self.grid_num_per_cell = np.concatenate([self.grid_num_per_cell, np.ones(points_tmp.shape[0]) * grid_num[d]])

            dem = dem_list[d]
            if not flat:
                grid_dem, grid_dem_bounds, grid_dem_d, grid_dem_path = read_raster(dem)
                if x_boundary[d] != 'auto' or y_boundary[d] != 'auto':

                #     srcwin =  list(((np.abs(np.array(self.bounds[d][0][:2])-np.array(grid_dem_bounds[0]))).astype(int) * 1 / np.abs(self.grid_size[d]**(1/3)))) \
                #             + [x.shape[0], y.shape[0]]
                #     print(srcwin)
                    cropped_dem = crop_raster(dem,x_boundary=self.bounds[d][:,0],y_boundary=self.bounds[d][:,1])
                    grid_dem, grid_dem_bounds, grid_dem_d, grid_dem_path = read_raster(cropped_dem,raster_path=grid_dem_path)

                dem = tuple([grid_dem, self.bounds[d][:, :2], self.intervals[d][:2], dem_list[d]])

                # print(read_raster(dem)[0],read_raster(grid_dem_path)[0])
                mask = []
                #                 points_topo = []
                xv, yv, zv = grid
                nx, ny, nz = len(xv), len(xv[0]), len(zv[0][0])
                # print(grid.shape,grid_dem.shape)
                for k in range(len(zv[0][0])):
                    for j in range(len(xv[0])):
                        for i in range(len(xv)):
                            if topo_layer[d] is not None:
                                if not central:
                                    if ((grid_dem[:, i, j][0] <= np.max(np.array(self.bounds)[d - 1, :, 0])) & (
                                                grid_dem[:, i, j][0] >= np.min(np.array(self.bounds)[d - 1, :, 0]))) & (
                                                (grid_dem[:, i, j][1] <= np.max(np.array(self.bounds)[d - 1, :, 1])) & (
                                                        grid_dem[:, i, j][1] >= np.min(
                                                        np.array(self.bounds)[d - 1, :, 1]))):
                                        if grid[:, i, j, k][2] >= np.min(np.array(self.bounds)[d - 1, :, 2]):
                                            mask.append(True)
                                        else:
                                            if grid_dem[:, i, j][2] >= grid[:, i, j, k][2] and grid_dem[:,i,j][2] - topo_layer[d] <= grid[:,i,j,k][2]:
                                                mask.append(False)
                                            # points_topo.append(grid[:,i,j,k])
                                            else:
                                                mask.append(True)
                                                # mask.append(False)
                                                # points_topo.append(grid[:,i,j,k])
                                    elif grid_dem[:, i, j][2] >= grid[:, i, j, k][2] and grid_dem[:,i,j][2] - topo_layer[d] <= grid[:,i,j,k][2]:
                                        mask.append(False)
                                    # points_topo.append(grid[:,i,j,k])
                                    else:
                                        mask.append(True)

                                else:
                                    if grid_dem[:, i, j][2] >= grid[:, i, j, k][2] and grid_dem[:,i,j][2] - topo_layer[d] <= grid[:,i,j,k][2]:
                                        mask.append(False)
                                    # points_topo.append(grid[:,i,j,k])
                                    else:
                                        mask.append(True)
                            else:
                                if not central:
                                    if ((grid_dem[:, i, j][0] <= np.max(np.array(self.bounds)[d - 1, :, 0])) & (
                                        grid_dem[:, i, j][0] >= np.min(np.array(self.bounds)[d - 1, :, 0]))) & (
                                        (grid_dem[:, i, j][1] <= np.max(np.array(self.bounds)[d - 1, :, 1])) & (
                                        grid_dem[:, i, j][1] >= np.min(np.array(self.bounds)[d - 1, :, 1]))):
                                        if grid[:, i, j, k][2] >= np.min(np.array(self.bounds)[d - 1, :, 2]):
                                            mask.append(True)
                                        else:
                                            if grid_dem[:, i, j][2] >= grid[:, i, j, k][2]:
                                                mask.append(False)
                                            # points_topo.append(grid[:,i,j,k])
                                            else:
                                                mask.append(True)
                                            # mask.append(False)
                                            # points_topo.append(grid[:,i,j,k])
                                    elif grid_dem[:, i, j][2] >= grid[:, i, j, k][2]:
                                        mask.append(False)
                                    # points_topo.append(grid[:,i,j,k])
                                    else:
                                        mask.append(True)

                                else:
                                    if grid_dem[:, i, j][2] >= grid[:, i, j, k][2]:
                                        mask.append(False)
                                    # points_topo.append(grid[:,i,j,k])
                                    else:
                                        mask.append(True)
                if central:
                    self.mask = np.array(mask)
                # self.points_topo = np.array(points_topo)
                else:
                    self.mask = np.concatenate([self.mask, np.array(mask)])
                    #                     self.points_topo = np.concatenate([self.points_topo,np.array(points_topo)])
            central = False
            dem_new_list.append(dem)

        if not flat:
            self.cell_centers = self.cell_centers[~self.mask]
            self.cell_size = self.cell_size[~self.mask]
            self.grid_num_per_cell = self.grid_num_per_cell[~self.mask]

        self.size = len(self.cell_centers)
        self.cell_centers = pd.DataFrame(self.cell_centers, columns=['x', 'y', 'z'])
        self.cell_centers['mesh_num'] = self.grid_num_per_cell
        # self.cell_centers = self.points
        # print(self.cell_centers)
        cell_xy_tuples = [tuple(x) for x in self.cell_centers[['x','y']].values]
        self.cell_centers_rev = pd.DataFrame(
                    np.array([self.cell_centers.z.values, np.array(self.cell_centers.index)]).T,
                    index=pd.MultiIndex.from_tuples(cell_xy_tuples), columns=['z', 'idx'], dtype=(np.float32, np.int32))
        self.cell_centers_rev.idx = self.cell_centers_rev.idx.astype(np.int32)
        self.cell_properties = None
        self.grid_num = grid_num
        # self.dem_list = dem_list
        self.dem_list = dem_new_list
        self.internal_boundaries = {}
        self.internal_regions = {}
        self.mesh_surface = None
        self.cell_depths = None
        self.corners_per_cell = None

    def create_hexahedral_mesh(self, grids_num=[0], points=None, mask=None):

        self.cells = pd.DataFrame(columns=[0, 1, 2, 3, 4, 5, 6, 7, 'mesh_num'])
        self.corners = pd.DataFrame(columns=['x', 'y', 'z', 'mesh_num'])
        corner_idx, cell_idx = 0, 0
        for d in grids_num:
            size = self.grid_size[d]
            print('Meshing grid number %i with cell size %.2f x %.2f x %.2f m' % (
            d, size ** (1 / 3), size ** (1 / 3), size ** (1 / 3)))

            # order = np.array([[-1, -1, -1], [1, -1, -1], [1, 1, -1], [-1, 1, -1], [-1, -1, 1], [1, -1, 1], [1, 1, 1],
            #                   [-1, 1, 1]]) * size ** (1 / 3) / 2
            # corner_list = np.array([self.points[self.grid_num_per_cell == d] + o for o in order])
            # corner_list = np.array([corner_list[:, c] for c in range(corner_list.shape[1])])
            corner_list = set_corners(self.cell_centers[self.grid_num_per_cell == d][['x','y','z']].values, size_xyz=[size ** (1 / 3), size ** (1 / 3), size ** (1 / 3)]).t
            corners = np.vstack({tuple(row) for row in corner_list.reshape(corner_list.shape[0] * corner_list.shape[1],
                                                                           corner_list.shape[2])})
            corn_tuples = [tuple(x) for x in corner_list.reshape(-1, 3)]
            del corner_list
            print('delete corner_list')
            points_tuples = [tuple(x) for x in corners]
            points_rev = pd.DataFrame(np.arange(len(corners)), index=pd.MultiIndex.from_tuples(points_tuples))
            del points_tuples
            print('delete points_tuples')
            cell_order = points_rev.loc[corn_tuples][0].values.reshape(-1, 8) + corner_idx
            del corn_tuples, points_rev
            print('delete points_rev')
            self.cells = self.cells.append(pd.DataFrame(index=np.arange(cell_idx, cell_idx + cell_order.shape[0]),
                                                        columns=[0, 1, 2, 3, 4, 5, 6, 7, 'mesh_num']))
            self.cells.loc[cell_idx:, [0, 1, 2, 3, 4, 5, 6, 7]] = cell_order
            self.cells.loc[cell_idx:, 'mesh_num'] = [d] * cell_order.shape[0]
            self.corners = self.corners.append(pd.DataFrame(index=np.arange(corner_idx, corner_idx + corners.shape[0]),
                                                            columns=['x', 'y', 'z', 'mesh_num']))
            self.corners.loc[corner_idx:, ['x', 'y', 'z']] = corners
            self.corners.loc[corner_idx:, 'mesh_num'] = [d] * corners.shape[0]
            corner_idx += self.corners.shape[0]
            cell_idx += self.cells.shape[0]

            #         self.cells.index = np.arange(self.cells.shape[0])
            #         self.corners.index = np.arange(self.corners.shape[0])



    def to_vtk(self, mesh_num='all', data=None):

        if mesh_num == 'all':
            corners = self.corners
            cells = self.cells
        else:
            corners = self.corners[self.corners['mesh_num'] == mesh_num]
            idx = self.corners[self.corners['mesh_num'] < mesh_num].shape[0]
            cells = self.cells[self.cells['mesh_num'] == mesh_num] - idx
            # print(cells.min())
        # if self.vtk is None:
        # Initialize the vtkPoints variable and set the number of points
        points_vtk = vtkPoints()
        points_vtk.SetDataTypeToDouble()
        points_vtk.SetNumberOfPoints(corners.shape[0])
        for f in enumerate(corners.values):
            points_vtk.InsertPoint(f[0], f[1][0], f[1][1], f[1][2])
        aHexahedronGrid = vtkUnstructuredGrid()
        aHexahedronGrid.SetPoints(points_vtk)

        for cell in enumerate(cells.values):
            aHexahedron = vtkHexahedron()
            for i in range(8):
                aHexahedron.GetPointIds().SetId(i, cell[1][
                    i])  # Cell point 0 corresponds to point 8 which was defined above
            aHexahedronGrid.InsertNextCell(aHexahedron.GetCellType(), aHexahedron.GetPointIds())

        if data is not None:
            cell_data = aHexahedronGrid.GetCellData()
            for col in data.columns:
                # #                     exec("print(data["+col+"])")
                # #                     exec(col + "_array = numpy_support.numpy_to_vtk(data[%s].values,array_type=vtk.VTK_FLOAT)"%(col))
                # #                     exec(col + "_array.SetName('"+col+"')")
                # #                     exec("cell_data.AddArray("+col+"_array)")
                data_array = numpy_support.numpy_to_vtk(data[col].values.ravel(), array_type=VTK_FLOAT)
                data_array.SetName(col)
                cell_data.AddArray(data_array)

        self.vtk = aHexahedronGrid
        return self.vtk

    #         elif self.vtk.GetCellData().GetNumberOfArrays() != len(data.columns) \
    #                 or (np.sort([self.vtk.GetCellData().GetArray(x).GetName() for x in range(self.vtk.GetCellData().GetNumberOfArrays())]) != np.sort(data.columns)).all():
    #             cell_data = self.vtk.GetCellData()
    #             for col in data.columns:
    #                 data_array = numpy_support.numpy_to_vtk(data[col].values,array_type=VTK_FLOAT)
    #                 data_array.SetName(col)
    #                 cell_data.AddArray(data_array)
    #             return self.vtk
    #         elif not np.array([(data[x].values == numpy_support.vtk_to_numpy(self.vtk.GetCellData().GetArray(y))).all()
    #                            for x,y in zip(np.sort(data.columns),np.sort([self.vtk.GetCellData().GetArray(z).GetName() for z in range(self.vtk.GetCellData().GetNumberOfArrays())]))]).all():
    #             cell_data = self.vtk.GetCellData()
    #             for col in data.columns:
    #                 data_array = numpy_support.numpy_to_vtk(data[col].values,array_type=VTK_FLOAT)
    #                 data_array.SetName(col)
    #                 cell_data.AddArray(data_array)
    #             return self.vtk
    #         else:
    #             return self.vtk
    # #             self.points = points

    def save(self, output_mesh, data=None, mesh_num=[0], output_type='vtk'):  # data = dataframe

        if isinstance(output_mesh, str):
            output_mesh = [''.join(np.insert(np.array(os.path.splitext(output_mesh)), 1, '_%i')) % d for d in mesh_num]

        for d in range(len(mesh_num)):
            if data is None:
                data_mesh = None
            elif isinstance(data, pd.DataFrame):
                data_mesh = data[data['mesh_num'] == d]
            else:
                data_mesh = data[d]

                if not isinstance(data_mesh, pd.DataFrame):
                    print('Data type not understood')
            if output_type == 'vtk':
                aHexahedronGrid = self.to_vtk(mesh_num=d, data=data_mesh)

            writer = vtkUnstructuredGridWriter()
            writer.SetInputData(aHexahedronGrid)
            writer.SetFileName(output_mesh[d])
            writer.Update()


    def set_internal_plane_boundary(self, dem_boundaries=[], name='SZ'):

        # if self.cell_properties is None:
        #     self.cell_properties = pd.DataFrame(np.zeros(len(self.cell_centers.index)), columns=[name],
        #                                         index=self.cell_centers.index)
        # else:
        #     self.cell_properties[name] = np.ones(len(self.cell_properties.index)) * 0

        boundary = pd.DataFrame()
        for d in self.grid_num:
            print("Setting internal plane boundary with name '%s' for grid number %i" % (name, d))
            if isinstance(dem_boundaries, str):
                dem_boundary_path = dem_boundaries
            else:
                dem_boundary_path = dem_boundaries[d]
            # size = self.grid_size[d]
            dem = self.dem_list[d]
            # grid_boundary, grid_boundary_bounds, grid_boundary_d, grid_boundary_path = read_raster(dem_boundary_path)
            # if size**(1/3) != np.abs(grid_boundary_d[0]):
            #     dem_boundary, grid_boundary_path = resample_raster_resolution(dem_boundary_path,
            #                                                                   outsize=size ** (1 / 3) / np.abs(
            #                                                                       grid_boundary_d[0])
            #                                                                   , output=grid_boundary_path)
            # else:
            #     dem_boundary = dem_boundary_path
            # grid, bounds, grid_d, grid_path = read_raster(dem)
            # x_off, y_off = find_offset_between_rasters(dem, grid_boundary_path)
            # if np.abs(x_off) >= 1 or np.abs(y_off) >= 1:
            #     dem_boundary = crop_raster(dem_boundary,srcWin = bounds[0]+[grid.shape[1],grid.shape[2]])
            #     x_off, y_off = find_offset_between_rasters(dem, dem_boundary)
            # dem_boundary,grid_boundary_path = translate_raster(dem_boundary,x_off,y_off,output=grid_boundary_path)
            if dem_boundary_path!=dem:
                dem_boundary = align_rasters(dem_boundary_path, dem)
            else:
                dem_boundary = dem_boundary_path
                grid_boundary_path = dem_boundary_path[-1]
            grid_boundary, grid_boundary_bounds, grid_boundary_d, grid_boundary_path = read_raster(dem_boundary,raster_path=grid_boundary_path)
            x_boundary, y_boundary, z_boundary = grid_boundary
            points_boundary = np.array([x_boundary.flatten(), y_boundary.flatten(), z_boundary.flatten()]).T

            points_boundary = points_boundary[(points_boundary[:, 2] != 0.0) & (points_boundary[:, 2] != 1.0)] ## check

            boundary_grid = pd.DataFrame(
                index=pd.MultiIndex.from_tuples([tuple(x) for x in points_boundary[:, :2]]), columns=['boundary'])

            boundary_grid.boundary = points_boundary[:, 2]
            boundary_grid_cell = pd.DataFrame(index=pd.MultiIndex.from_tuples(
                [tuple(np.array(idx) - np.array(grid_boundary_d) / 2) for idx in
                 self.cell_centers_rev[self.grid_num_per_cell == d].index.drop_duplicates()]))
            boundary_grid_cell['boundary'] = np.zeros(boundary_grid_cell.shape[0])
            boundary_grid_cell = boundary_grid.loc[boundary_grid_cell.index]
            del boundary_grid
            # print(grid_boundary_d,boundary_grid_cell)
            # self.boundary_grid_cell = boundary_grid_cell
            # self.boundary_grid = boundary_grid
            boundary_grid_cell.index = pd.MultiIndex.from_tuples(
                [tuple(np.array(idx)) for idx in
                 self.cell_centers_rev[self.grid_num_per_cell == d].index.drop_duplicates()])
            cell_boundary = self.cell_centers_rev.copy()
            cell_boundary['boundary'] = np.ones(cell_boundary.shape[0]) * np.nan

            cell_boundary['boundary'].iloc[cell_boundary.loc[boundary_grid_cell.index].groupby(
                level=[0, 1]).first().idx.values] = boundary_grid_cell.boundary
            cell_boundary.loc[boundary_grid_cell.index] = cell_boundary.loc[boundary_grid_cell.index].fillna(method='ffill')
            #print(cell_boundary)

            boundary = boundary.append(cell_boundary[self.grid_num_per_cell == d])
            del boundary_grid_cell
        boundary.sort_values('idx', inplace=True)

        self.internal_boundaries[name] = boundary

    def set_distance_to_internal_boundary(self,boundary_name,name=''):

        if name == '':
            name = boundary_name
        if self.cell_properties is None:
            self.cell_properties = pd.DataFrame(np.zeros(len(self.cells.index)), columns=[name], index=self.cells.index)
        else:
            self.cell_properties[name] = np.ones(len(self.cell_properties.index)) * 0

        boundary = self.internal_boundaries[boundary_name]
        distance = boundary.z - boundary.boundary
        self.cell_properties[name] = distance.values

    def set_below_above_internal_boundary(self,boundary_name,name='',below=1,above=0):

        if name == '':
            name = boundary_name
        if self.cell_properties is None:
            self.cell_properties = pd.DataFrame(np.zeros(len(self.cell_centers.index)), columns=[name], index=self.cell_centers.index)
        else:
            self.cell_properties[name] = np.ones(len(self.cell_properties.index)) * 0

        boundary = self.internal_boundaries[boundary_name]
        distance = boundary.z - boundary.boundary
        status = distance.values
        print(status,len(status))
        status[distance.values>=0] = above
        status[distance.values < 0] = below

        self.cell_properties[name] = status

    def get_mesh_surface(self,from_dem=False):
        if from_dem:
            self.set_internal_plane_boundary(dem_boundaries=self.dem_list, name='surface')
            self.mesh_surface = self.internal_boundaries['surface'].boundary
        else:
            self.mesh_surface = pd.DataFrame()
            for d in self.grid_num:
                mesh_surface_cell = self.cell_centers_rev[self.grid_num_per_cell == d].copy()
                mesh_surface_cell['surface'] = self.cell_centers_rev[self.grid_num_per_cell == d].groupby(level=[0, 1], sort=False)['z'].transform(max)
                mesh_surface_cell.surface = mesh_surface_cell.surface + self.cell_size[self.grid_num_per_cell == d]**(1/3) / 2
                self.mesh_surface = self.mesh_surface.append(mesh_surface_cell)#drop('z',axis=1)
            self.mesh_surface = self.mesh_surface.surface
    def get_cell_depth(self):
        self.get_mesh_surface(from_dem=True)
        self.cell_depths = pd.DataFrame(np.zeros(len(self.cell_centers.index)), columns=['depth'], index=self.cell_centers.index)
        self.cell_depths.depth = self.mesh_surface.values - self.cell_centers.z.values
        self.cell_depths['depth_norm'] = self.cell_depths.depth - (self.cell_depths.depth % self.cell_size ** (1 / 3))

    def set_regions_from_surface_raster(self, surface_raster_paths, name='geology', rename_properties=False, rest=0,
                                        properties={1: 'limestone'}, dip=0, direction=0):
        if self.mesh_surface is None:
            self.get_mesh_surface()

        if self.cell_properties is None:
            self.cell_properties = pd.DataFrame(np.zeros(len(self.cell_centers.index)), columns=[name], index=self.cell_centers.index)
        else:
            self.cell_properties[name] = np.ones(len(self.cell_properties.index)) * rest

        new_region = pd.DataFrame()
        for d in self.grid_num:
            print("Setting region from surface with name '%s' for grid number %i" % (name, d))
            dem = self.dem_list[d]



            if dip == 0 and direction == 0:
                surface_raster_path = align_rasters(surface_raster_paths, dem)
                regions = map_regions_boundaries(surface_raster_path, self.mesh_surface)
                self.regions = regions
                if len(regions[regions.region != 0].index) == 0:
                    pass
                else:
                    cell_region = self.cell_centers_rev.copy()
                    cell_region['region'] = np.ones(cell_region.shape[0]) * np.nan
                    cell_region['region'].iloc[cell_region.loc[regions.index].drop_duplicates().idx.values] = regions.region
                    cell_region = cell_region[self.grid_num_per_cell == d]
                    new_region = new_region.append(cell_region)

            else:

                surface_raster_path = align_rasters(surface_raster_paths, dem, frame=[80, 80])    ## TODO: improve frame
                if self.cell_depths is None:
                    self.get_cell_depth()
                depth_list = self.cell_depths.depth_norm[self.grid_num_per_cell == d].drop_duplicates().values
                print(depth_list)
                for depth in depth_list:  # [-2:]:#[:-1]:
                    print("Reaching depth %f m" % (depth))
                    translated_raster = translate_raster_old(surface_raster_path, dip=dip, direction=direction, depth=depth)
                    cell_list = np.array(list(set(self.cell_centers_rev[self.grid_num_per_cell == d].idx.values).intersection(
                        set(self.cell_depths[self.grid_num_per_cell == d][self.cell_depths[self.grid_num_per_cell == d].depth_norm == depth].index))))
                    # print(depth,cell_list.shape[0],self.cell_depths[self.grid_num_per_cell == d][self.cell_depths[self.grid_num_per_cell == d].depth == depth].shape[0])

                    cell_region = map_regions_boundaries(translated_raster,
                                                     self.cell_centers_rev.iloc[cell_list])  # self.mesh_surface)
                    new_region = new_region.append(cell_region)
                new_region.sort_values('idx', inplace=True)
        self.internal_regions[name] = new_region

    def set_internal_regions(self, bottom=None, top=None, name='cavity'):

        new_region = self.cell_centers_rev.copy()
        new_region['region'] = np.zeros(new_region.shape[0])
        for d in self.grid_num:
            print("Setting internal region with name '%s' for grid number %i" % (name, d))
            dem = self.dem_list[d]
            dem_bottom_path = align_rasters(bottom, dem)
            dem_top_path = align_rasters(top, dem)
            region_idx = find_cavities(dem_bottom_path, dem_top_path, self.cell_centers_rev[self.grid_num_per_cell == d])
            new_region.region.iloc[region_idx] = 1
        new_region.sort_values('idx', inplace=True)
        self.internal_regions[name] = new_region

    def set_corners_from_centers(self):
        # self.corners = pd.DataFrame(np.zeros(len(self.cell_centers.index)), columns=np.arange(8), index=self.cell_centers.index)
        self.corners_per_cell = pd.DataFrame()
        for d in self.grid_num:
            self.corners_per_cell = self.corners_per_cell.append(pd.DataFrame([*zip(*set_corners(self.cell_centers[self.grid_num_per_cell == d][['x','y','z']].values,size_xyz=[self.grid_size[d]**(1/3)]*3))]),ignore_index=True)

    # def set_gravitational_attraction(self,center,methods={0:'prisms'},name='g',rho=1000,G=6.67*10**(-11)):
    #     if isinstance(methods,str):
    #         methods = {k:v for k,v in zip(self.grid_num,[methods]*len(self.grid_num))}
    #     if self.cell_properties is None:
    #         self.cell_properties = pd.DataFrame(np.zeros(len(self.cell_centers.index)), columns=[name], index=self.cell_centers.index)
    #     else:
    #         self.cell_properties[name] = np.ones(len(self.cell_properties.index)) * 0
    #     for d in self.grid_num:
    #         print("Setting gravitational attraction with method '%s' for grid number %i" % (methods[d], d))
    #         if methods[d] == 'prisms':
    #             # self.cell_properties[name].iloc[self.cell_centers[self.grid_num_per_cell == d].index] = \
    #             #     self.cell_properties.iloc[self.cell_centers[self.grid_num_per_cell == d].index].apply(lambda row: cube_gravitational_attraction(center,
    #             #                                                  set_corners(self.cell_centers[self.grid_num_per_cell == d].iloc[row.name][['x','y','z']].values,size_xyz=[self.grid_size[d]**(1/3)]*3).T,rho=rho,G=G),
    #             #        axis=1)
    #             if self.corners_per_cell is None:
    #                 self.set_corners_from_centers()
    #
    #             self.cell_properties[name].iloc[self.cell_centers[self.grid_num_per_cell == d].index] = \
    #                 self.cell_properties.iloc[self.cell_centers[self.grid_num_per_cell == d].index].apply(
    #                     lambda row: cube_gravitational_attraction(center,self.corners_per_cell[self.grid_num_per_cell == d].iloc[row.name], rho=rho, G=G),
    #                     axis=1)
    #
    #         elif methods[d] == 'point_mass':
    #             gravi = point_mass_df(center,self.cell_centers[self.grid_num_per_cell == d],size=self.cell_size[self.grid_num_per_cell == d],rho=rho,G=G)
    #             self.cell_properties[name].iloc[self.cell_centers[self.grid_num_per_cell == d].index]= gravi
    #
    #         elif methods[d] == 'mac_millan':
    #             gravi = mac_millan_df(center,self.cell_centers[self.grid_num_per_cell == d],size=self.cell_size[self.grid_num_per_cell == d],rho=rho,G=G)
    #             self.cell_properties[name].iloc[self.cell_centers[self.grid_num_per_cell == d].index]= gravi
    def set_gravitational_attraction(self,center,methods={0:'prisms'},name='g',rho=1000,G=6.67*10**(-11)):
        if isinstance(methods,str):
            methods = {k:v for k,v in zip(self.grid_num,[methods]*len(self.grid_num))}
        if self.cell_properties is None:
            self.cell_properties = pd.DataFrame(np.zeros(len(self.cell_centers.index)), columns=[name], index=self.cell_centers.index)
        else:
            self.cell_properties[name] = np.ones(len(self.cell_properties.index)) * 0
        for d in self.grid_num:
            if methods[d] == 'prisms':
                self.cell_properties[name].iloc[self.cell_centers[self.grid_num_per_cell == d].index] = \
                    self.cell_properties.iloc[self.cell_centers[self.grid_num_per_cell == d].index].apply(lambda row: cube_gravitational_attraction(center,
                                                                 set_corners(self.cell_centers[self.grid_num_per_cell == d].iloc[row.name][['x','y','z']].values,size_xyz=[self.grid_size[d]**(1/3)]*3),rho=rho,G=G),
                       axis=1)
            elif methods[d] == 'point_mass':
                gravi = point_mass_df(center,self.cell_centers[self.grid_num_per_cell == d],size=self.cell_size[self.grid_num_per_cell == d],rho=rho,G=G)
                self.cell_properties[name].iloc[self.cell_centers[self.grid_num_per_cell == d].index]= gravi
